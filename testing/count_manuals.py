import json, pprint


def count_manuals(json_file):
    ''' get the baseline json file and count manuals in each package '''

    with open(json_file) as f:
        items = json.load(f)
    
    for idx, manual in enumerate(items):
        print(idx + 1, '-', manual['man'])
        
count_manuals('cat_baseline.json')
