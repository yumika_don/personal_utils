import json, csv
import xml.etree.ElementTree as etree
from pprint import pprint

# 1 Byte == 0.000000001 GB
DATA_SIZE = {'byte_gig': 0.000000001, 'byte_mega': 0.000001, 'byte_kilo': 0.001}

def convert_to_csv(json_file, file_name='dumped.csv'):
    with open(json_file, encoding='utf-8') as jsonObj:
        stuff_in_json = json.load(jsonObj)
        header = [item for item in stuff_in_json[0]]
        contents = [list(item.values()) for item in stuff_in_json]

        with open(file_name, 'w', newline='', encoding='utf-8') as csvObj:
            csv_writer = csv.writer(csvObj)
            csv_writer.writerow(header)

            for item in contents:
                csv_writer.writerow(item)


def xml_to_list(xml_file):
    tree = etree.parse(xml_file)
    root = tree.getroot()
    return [(child.attrib['revisionId'], child.attrib['docNumber']) for child in root]


def xml_to_csv(xml_file, file_name='dumped_xml.csv'):
    tree = etree.parse(xml_file)
    root = tree.getroot()
    attributes = [child.attrib for child in root]
    header = ['Document Number', 'File Name', 'External URL', 'Revision_id']
    with open(file_name, 'w', newline='', encoding='utf-8') as csvObj:
        csv_writer = csv.writer(csvObj)
        csv_writer.writerow(header)
        for item in attributes:
            if item.get('externalURL'):
                csv_writer.writerow([item['docNumber'], item['filename'], item['externalURL'], item['revisionId']])


def read_book_id(csv_file):
    with open(csv_file) as csvObj:
        csv_reader = csv.reader(csvObj)
        header = next(csvObj)
        return [(row[21].split('-')[4], row[2]) for row in csv_reader if 'SRC' not in row[21]]


def find_missing_ids():
    tml = set(read_book_id('sync_manifest.csv'))
    offline = set(xml_to_list('revisions.xml')[1])
    diff = offline.difference(tml)
    with open('missing_documents.csv', 'w', newline='', encoding='utf-8') as csvObj:
        header = ['revisionId', 'docNumber']
        csv_writer = csv.writer(csvObj)
        csv_writer.writerow(header)

        for item in diff:
            csv_writer.writerow([item[0], item[1]])
    return diff, tml, offline


xml_to_csv('revisions.xml')
# pprint(find_missing_ids()[2])
# print(len(find_missing_ids()[2]))

# pprint(xml_to_list('revisions.xml')[1])
# pprint(read_book_id('sync_manifest.csv'))

# book_uuid = read_book_id('sync_manifest.csv')
# pprint(book_uuid)
# print(len(book_uuid))

# xml_revisionIds = xml_to_list('revisions.xml')
# pprint(xml_revisionIds)
# print(len(xml_revisionIds))