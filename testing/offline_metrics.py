import csv, pprint

def filter_by_size():
    input_file = open('Triexport.csv')
    input_file_reader = csv.reader(input_file)
    header = next(input_file_reader)
    new_header = []
    
    for col in header:
        if col in ('COMPANY_CODE', 'TRIGGERED_DATE', 'CATEGORY', 'CREATED_DATE', 'BACKUP_PKG_SIZE', 'TIME_TAKEN'):
            new_header.append(col)

    output_file = open('output.csv', 'w', newline='')
    output_file_writer = csv.writer(output_file)
    output_file_writer.writerow(new_header)
    
    for row in input_file_reader:
        if row[1] == 'JAL':
            output_file_writer.writerow([row[1], row[2], row[4], row[7], row[13], row[14]])
    
filter_by_size()