import json

# 1 Byte == 0.000000001 GB
DATA_SIZE = 0.000000001

def calc_filesize(json_file):
    ''' calculate the file size of a zip package '''

    with open(json_file, 'r') as f_obj:
        items = json.load(f_obj)

    total_size_byte = 0
    total_size_gig = 0

    for book in items:
        file_size = int(book['file_size'])
        total_size_byte = total_size_byte + file_size

    total_size_gig = '{:.2f}'.format(total_size_byte * DATA_SIZE)

    return (total_size_byte, total_size_gig)


def book_size(json_file):
    ''' display each book and its file_size '''

    with open(json_file, 'r') as f_obj:
        items = json.load(f_obj)

    template = '{0:5} ({1}) {2} => {3:,} Bytes - {4} - rev num {5}\n'

    with open('dumped_info.txt', 'w', encoding='utf-8') as f_obj:
        for idx, book in enumerate(items):
            print(idx + 1, '-', template.format(book['display_title'], book['doc_abbreviation'],
        book['doc_number'], int(book['file_size']), book['book_uuid'], book['revision_number'], book['revision_date'], book['model_major'], book['model_minor']), file=f_obj)

byte, gig = calc_filesize('sync_manifest.json')

book_size('sync_manifest.json')

print('The SUM of all books is: {0:,} Bytes == {1}GB'.format(byte, gig))
