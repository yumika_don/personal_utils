# -*- coding: utf-8 -*-
"""
Created on Sat Oct 26 17:23:33 2019

@author: TriNguyen
"""

import requests
from bs4 import BeautifulSoup
import time

url = 'https://www5.series9.to/film/godless-season-1/watching.html?ep=3'
template = 'https://www5.series9.to/film/godless-season-1/watching.html?ep={}'
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0'}

for i in range(3, 8):
    video_link = template.format(i)
    video_response = requests.get(video_link, headers=headers)
    html = video_response.text
    soup = BeautifulSoup(html, 'lxml')
    video_src = soup.find('iframe').get('src')
    video_url = 'https:'+video_src
    print(video_url)
    time.sleep(3)
