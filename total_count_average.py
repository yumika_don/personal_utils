numbers_from_user = []

while True:
	user_input = input('Enter a number (enter \'done\' to quit.): ')
	if user_input == 'done':
		break
		
	try:
		user_input = int(user_input)
		numbers_from_user.append(user_input)
		
		total = sum(numbers_from_user)
		count = len(numbers_from_user)
		average = total / count
		minimum = min(numbers_from_user)
		maximum = max(numbers_from_user)
	except:
		if user_input.isnumeric == False:
			continue
		print('>>Invalid input. Please enter a numeric value.')
	
print('Total: ', total)
print('Count: ', count)
print('Average: ', average)
print('Minimum: ', minimum)
print('Maximum: ', maximum) 


