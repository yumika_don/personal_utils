number_range = list(range(1, 101))

for indx, number in enumerate(number_range):
    if number % 4 == 0:
        print(number, '-', 'Go')
    if number % 5 == 0:
        print(number, '-', 'Figure')
    if (number % 4 == 0 and number % 5 == 0):
        print(number, '-', 'GoFigure')
        
print(number_range)
#e0a37f85eb7b199eae09a0dc08959176