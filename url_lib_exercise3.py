#use urllib to replicate the previous exercise of retrieving the document from a URL.
#display up to 3000 characters, and count the overal number of characters in the document

import urllib.request, urllib.parse, urllib.error

url = input('Enter a URL: ')

open_url = urllib.request.urlopen(url)

char_count = dict()

while True:
	info = open_url.read(3000)
	list_info = list(info)
	for char in list_info:
		char_count.setdefault(char, 0)
		char_count[char] = char_count[char] + 1
	if len(info) < 1:
		break
	print(info[:3000])

list_info_values = list(char_count.values())
print(list_info_values)
print(sum(list_info_values))