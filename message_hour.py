# count the distribution of the hour of the day for each of the messages.
# pull the hour from the "From" line by finding the time string and then splitting that string into parts using the colon character.
# print out the counts on per line sorted by the hour then number of occurrences.

file_name = input('Enter a file name: ')

try:
	file_handle = open(file_name)
except:
	print(file_name, ' does not exist.')
	
hour_dict = dict()

for line in file_handle:
	line = line.rstrip()
	if line.startswith('From'):
		words = line.split()
		if words[0] != 'From' or len(words) == 0:
			continue
		else:
			hour, sec, ms = words[5].split(':')
			hour_dict.setdefault(hour, 0)
			hour_dict[hour] = hour_dict[hour] + 1

hour_list = list()
for key, val in list(hour_dict.items()):
	hour_list.append((key, val))

hour_list.sort()

for key, val in hour_list:
	print(key, val)