import os, shutil

def copy_files(src_directory, dst_directory, file_extention = None):
	''' move files from one source to another '''
	
	for folder_name, sub_folders, file_names in os.walk(src_directory):
		for file_name in file_names:
			if file_name.endswith(file_extention):
				shutil.copy(os.path.join(folder_name, file_name), dst_directory)
				
copy_files('F:\\', 'C:\\my_songs', '.mp3')
print('Complete!!!')