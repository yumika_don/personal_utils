class ListInherited:

    ''' 
        Use dir() to collect both instance attrs and names inherited from
        its classes. '''

    def __attrnames(self):
        result = ''
        for attr in dir(self):
            if attr[:2] == '__' and attr[-2:] == '__':
                result += '\t{}\n'.format(attr)
            else:
                result += '\t{}={}\n'.format(attr, getattr(self, attr))
        return result

    def __str__(self):
        return '<Instance of {}, address {}: \n{}>'.format(
        self.__class__.__name__, id(self), self.__attrnames())