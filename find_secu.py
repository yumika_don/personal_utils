#get file name

file_name = input('Enter file name: ')
try:
	file_handle = open(file_name)
except:
	print('File not found!!!')
	exit()
	
count = 0

for line in file_handle:
	if line.startswith('SECU//'):
		count = count + 1
		line = line.rstrip()
		print(line)
		
file_handle.close()
		
print('There are', count, 'items')