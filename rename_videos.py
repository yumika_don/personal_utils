import datetime
import os
import shutil


def rename_home_videos(src_dir, dest_dir=None):
    ''' Rename all home videos files in the src_dir based on os.stat st_mtime.
        If the dest_dir is given then use the shutil module to copy to the
        dest_dir
    '''

    videos_to_rename = [video for video in os.listdir(src_dir) 
                       if video.endswith('.MTS')
                       ]

    for video in videos_to_rename:
        modified_date = datetime.datetime.fromtimestamp(
            os.stat(os.path.join(src_dir, video)).st_mtime)
        name, ext = os.path.splitext(video)
        new_name = '{}_{}_{}_{}_{}_{}'
        if dest_dir is None:
#             print(os.path.join(src_dir, video),
#                  os.path.join(
#                     src_dir,
#                     new_name.format(
#                     modified_date.month,
#                     modified_date.day,
#                     modified_date.year,
#                     modified_date.hour,
#                     modified_date.minute,
#                     modified_date.second)+ext
#                  ))
            os.rename(
                os.path.join(src_dir, video),
                os.path.join(
                    src_dir,
                    new_name.format(
                    modified_date.month,
                    modified_date.day,
                    modified_date.year,
                    modified_date.hour,
                    modified_date.minute,
                    modified_date.second)+ext
                ))
        else:
            shutil.copy(
                os.path.join(src_dir, video),
                os.path.join(
                    dest_dir,
                    new_name.format(
                    modified_date.month,
                    modified_date.day,
                    modified_date.year,
                    modified_date.hour,
                    modified_date.minute,
                    modified_date.second)+ext
                ))
