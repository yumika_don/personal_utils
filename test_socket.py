#prompt user for the URL so it can read any we page.
#user split('/') to break the URL into its component parts so you can extract the host name for the socket connect call.
#Add error checking using try and except to handle the condition where the user enters improperly formatted or non-existent URL.
#count the number of characters it has received and stop displaying any text after it has shown 3000 characters. The program should retrieve the entire document.
#Then count the total number of characters and display the count of the number of characters at the end of the document.

import socket
import time

url = input('Enter a URL: ')
print(url)
split_html = url.split('/')
print(split_html)

count = 0

try:

	mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	mysock.connect((split_html[2], 80))
	cmd = 'GET ' + url + ' HTTP/1.0\n\n'
	mysock.sendall(cmd.encode())
	
	while True:
		data = mysock.recv(600)
		time.sleep(0.25)		
		if count >= 3000 or len(data) < 1:
			break
		count = count + len(data)
		print(len(data), count, data.decode())
	mysock.close()
	
except:
	if split_html[0] != 'http:' or split_html[2].find('.') == -1:
		print(url, ' is not recognized, or it does not exist.')

print(count)
