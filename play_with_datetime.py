Python 3.6.0 (v3.6.0:41df79263a11, Dec 23 2016, 07:18:10) [MSC v.1900 32 bit (Intel)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> import datetime
>>> test = "loadEvents('3217812','10387','1334','2019-04-27 13:00:00','schedule')"
>>> split_test = test.split(',')
>>> split_test
["loadEvents('3217812'", "'10387'", "'1334'", "'2019-04-27 13:00:00'", "'schedule')"]
>>> time_part = split_test[3]
>>> time_part
"'2019-04-27 13:00:00'"
>>> time_part = time_part.strip("'")
>>> time_part
'2019-04-27 13:00:00'
>>> dt = datetime.datetime
>>> dt
<class 'datetime.datetime'>
>>> dt = strptime(time_part, '%Y-%m-%dT%H:%M:%S%z')
Traceback (most recent call last):
  File "<pyshell#10>", line 1, in <module>
    dt = strptime(time_part, '%Y-%m-%dT%H:%M:%S%z')
NameError: name 'strptime' is not defined
>>> dt = datetime.datetime.strptime(time_part, '%Y-%m-%dT%H:%M:%S%z')
Traceback (most recent call last):
  File "<pyshell#11>", line 1, in <module>
    dt = datetime.datetime.strptime(time_part, '%Y-%m-%dT%H:%M:%S%z')
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 565, in _strptime_datetime
    tt, fraction = _strptime(data_string, format)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 362, in _strptime
    (data_string, format))
ValueError: time data '2019-04-27 13:00:00' does not match format '%Y-%m-%dT%H:%M:%S%z'
>>> time_part
'2019-04-27 13:00:00'
>>> split_time_part = time_part.split(' ')
>>> split_time_part
['2019-04-27', '13:00:00']
>>> dt = datetime.datetime.strptime(time_part, '%Y-%m-%d')
Traceback (most recent call last):
  File "<pyshell#15>", line 1, in <module>
    dt = datetime.datetime.strptime(time_part, '%Y-%m-%d')
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 565, in _strptime_datetime
    tt, fraction = _strptime(data_string, format)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 365, in _strptime
    data_string[found.end():])
ValueError: unconverted data remains:  13:00:00
>>> dt = datetime.datetime.strptime(time_part[0], '%Y-%m-%d')
Traceback (most recent call last):
  File "<pyshell#16>", line 1, in <module>
    dt = datetime.datetime.strptime(time_part[0], '%Y-%m-%d')
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 565, in _strptime_datetime
    tt, fraction = _strptime(data_string, format)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 362, in _strptime
    (data_string, format))
ValueError: time data '2' does not match format '%Y-%m-%d'
>>> dt = datetime.datetime.strptime(time_part[0], '%Y-%m-%d')
Traceback (most recent call last):
  File "<pyshell#17>", line 1, in <module>
    dt = datetime.datetime.strptime(time_part[0], '%Y-%m-%d')
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 565, in _strptime_datetime
    tt, fraction = _strptime(data_string, format)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 362, in _strptime
    (data_string, format))
ValueError: time data '2' does not match format '%Y-%m-%d'
>>> time_part
'2019-04-27 13:00:00'
>>> dt = datetime.datetime.strptime(time_part, '%Y-%m-%d %H:%S:%S')
Traceback (most recent call last):
  File "<pyshell#19>", line 1, in <module>
    dt = datetime.datetime.strptime(time_part, '%Y-%m-%d %H:%S:%S')
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 565, in _strptime_datetime
    tt, fraction = _strptime(data_string, format)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 345, in _strptime
    format_regex = _TimeRE_cache.compile(format)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\_strptime.py", line 275, in compile
    return re_compile(self.pattern(format), IGNORECASE)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\re.py", line 233, in compile
    return _compile(pattern, flags)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\re.py", line 301, in _compile
    p = sre_compile.compile(pattern, flags)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\sre_compile.py", line 562, in compile
    p = sre_parse.parse(p, flags)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\sre_parse.py", line 856, in parse
    p = _parse_sub(source, pattern, flags & SRE_FLAG_VERBOSE, False)
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\sre_parse.py", line 415, in _parse_sub
    itemsappend(_parse(source, state, verbose))
  File "C:\Users\TriNguyen\AppData\Local\Programs\Python\Python36-32\lib\sre_parse.py", line 757, in _parse
    raise source.error(err.msg, len(name) + 1) from None
sre_constants.error: redefinition of group name 'S' as group 6; was group 5 at position 141
>>> dt = datetime.datetime.strptime(time_part, '%Y-%m-%d %H:%M:%S')
>>> dt
datetime.datetime(2019, 4, 27, 13, 0)
>>> formatted_dt = datetime.datetime.strftime('%d-%b-%Y %I:%M%p')
Traceback (most recent call last):
  File "<pyshell#22>", line 1, in <module>
    formatted_dt = datetime.datetime.strftime('%d-%b-%Y %I:%M%p')
TypeError: descriptor 'strftime' requires a 'datetime.date' object but received a 'str'
>>> formatted_dt = datetime.datetime(dt)
Traceback (most recent call last):
  File "<pyshell#23>", line 1, in <module>
    formatted_dt = datetime.datetime(dt)
TypeError: an integer is required (got type datetime.datetime)
>>> dt
datetime.datetime(2019, 4, 27, 13, 0)
>>> formatted_dt = datetime.datetime(2019, 4, 27, 13, 0)
>>> formatted_dt = formatted_dt.strftime('%d-%b-%Y %I:%M%p')
>>> formatted_dt
'27-Apr-2019 01:00PM'
>>> dt
datetime.datetime(2019, 4, 27, 13, 0)
>>> dt.tm_year
Traceback (most recent call last):
  File "<pyshell#29>", line 1, in <module>
    dt.tm_year
AttributeError: 'datetime.datetime' object has no attribute 'tm_year'
>>> print(datetime.datetime.now())
2019-06-15 15:55:53.593269
>>> now = datetime.datetime.now()
>>> now.tm_year
Traceback (most recent call last):
  File "<pyshell#32>", line 1, in <module>
    now.tm_year
AttributeError: 'datetime.datetime' object has no attribute 'tm_year'
>>> import time
>>> now = time.now()
Traceback (most recent call last):
  File "<pyshell#34>", line 1, in <module>
    now = time.now()
AttributeError: module 'time' has no attribute 'now'
>>> now = time.ctime
>>> now
<built-in function ctime>
>>> today = datetime.date.today()
>>> today
datetime.date(2019, 6, 15)
>>> today.time
Traceback (most recent call last):
  File "<pyshell#39>", line 1, in <module>
    today.time
AttributeError: 'datetime.date' object has no attribute 'time'
>>> 
