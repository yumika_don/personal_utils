#Write a simple program to simulate the operation of the grep command on Unix.
#Ask the user to enter a regular expression and count the number of lines that matched the regular expression

# Step 1: open the the mbox.txt file
# Step 2: Have user enter a regular expression
# Step 3: Set a counter to print out number of lines that matched
# Step 4: Write a for loop to find matched lines based on the regular expression that the user enters.

import re

user_regular_expression = input('Enter a regular expression: ') #store user's regular expression in user_regular_expression variable
print('You\'ve entered the following: ', user_regular_expression)
open_file = open('mbox-short.txt')

number_of_lines = 0

for each_line in open_file:
	each_line = each_line.rstrip()
	if re.search(user_regular_expression, each_line):
		number_of_lines = number_of_lines + 1
		print(each_line)

print('mbox.txt had ', number_of_lines, ' lines that matched ', user_regular_expression)