#change this program to extract and count p tags from the retrieved HTML document and display the count as output of the program.
#do not display paragraph text.

import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl
import re

ctx =ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter - ')
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')

title_tag = soup.html.head.title
print(title_tag)
print(len(soup('p')), ' <p> tags.')

print(soup('em'))
print(len(soup('em')))

for num, em in enumerate(soup('em')):
	print(num, ' - ' , em)
	

# tags = soup('p')
# p_count = 0
# p_tag = dict()
# for tag in tags:
	# p_count = p_count + 1
	# p_tag.setdefault(tag, 0)
	# p_tag[tag] = p_tag[tag] + 1 
	
# print('There are ',p_count, '<p> tags in the URL you entered.')
# print(p_tag)