# -*- coding: utf-8 -*-
"""
Created on Sat Aug 17 01:01:14 2019

@author: pr733d
"""

import requests
import time
from bs4 import BeautifulSoup
from urllib.parse import urlparse
from pprint import pprint


def get_rapidgator_links(url):
    res = requests.get(url)
    res.raise_for_status()
    parsed_url = urlparse(url)

    soup = BeautifulSoup(res.text, 'html.parser')
    if 'learningdl.net' in parsed_url[1]:
        main = soup.find('main')
        a_tags = main.find_all('a')
        links = [
                link.get('href') for link in a_tags
                if link.get('href') is not None
                ]
    else:
        li_tags = soup.select('ol li')
        links = [link.text for link in li_tags]
    all_rapid_links = []
    active_links = []
    dead_links = []
    for link in links:
        if 'rapidgator' in link:
            all_rapid_links.append(link)
    for link in all_rapid_links:
        res = requests.get(link)
        res.raise_for_status()
        headers = res.headers
        header_cookie = headers.get('Set-Cookie')
        if 'download_url' in header_cookie:
            active_links.append(link)
        elif 'fnf=deleted' in header_cookie:
            dead_links.append(link)
        time.sleep(4)
    return all_rapid_links, active_links, dead_links


# =============================================================================
# def get_link_from_learningdl(url):
#     response = requests.get(url)
#     response.raise_for_status()
#     soup = BeautifulSoup(response.text, 'lxml')
#     main = soup.find('main')
#     a_tags = main.find_all('a')
#     return [
#             link.get('href') for link in a_tags if link.get('href') is not None
#             ]
# =============================================================================


results = get_rapidgator_links('https://learningdl.net/customer-analytics-in-python-2020-udemy/')
all_rapid_links, active_links, dead_links = results
for link in active_links:
    print(link)
# =============================================================================
# rapid_links = get_link_from_learningdl('http://learningdl.net/pyqt-desktop-apps-with-python/')
# for link in rapid_links:
#     if 'rapidgator' in link:
#         print(link)
# =============================================================================
