file_name = input('Enter the file name: ')
try:
	file_handle = open('E:\\STUFF\\Python Programs\\'+file_name)
except:
	if file_name.endswith('.txt') == False:
		print(file_name.upper(), ' - You have been punk\'d!' )
	else:
		print('File cannot be opened: ', file_name)
	exit()

count = 0
for line in file_handle:
	if line.startswith('Subject:'):
		count = count + 1

print('There were ', count, ' subject lines in ', file_name)