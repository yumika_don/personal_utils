# -*- coding: utf-8 -*-

import requests
from datetime import datetime
from pytz import timezone
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString
from pprint import pprint


class LiveSoccerTV:

    URL = 'https://www.livesoccertv.com/schedules/'
    Major_Leagues = {'Italy - Serie A': '39',
                     'England - Premier League': '6',
                     'Spain - La Liga': '8'}

    pacific = timezone('US/Pacific')
    est = timezone('US/Eastern')

    def __init__(self, date=None, league=None):
        ''' date format is, for example, yyyy-mm-dd
            then convert it to yyyy-Name of month-dd to append to URL '''

        if date is None:
            self.date = datetime.now()
            self.date = datetime.strftime(self.date, '%Y-%m-%d')
            self._url = LiveSoccerTV.URL+self.date+'/'
            self._html = requests.get(self._url)
            self._html.raise_for_status()
        else:
            self.date = date
            self._url = LiveSoccerTV.URL+self.date+'/'
            self._html = requests.get(self._url)
            self._html.raise_for_status()

        self.soup = BeautifulSoup(self._html.text, 'lxml')
        self.table_schedules = self.soup.find('table', {'class': 'schedules'})
        self.date_header = self.soup.select('#leftcol_white > h1')
        self.match_rows = self.table_schedules.find_all(
                'tr', {'class': ['', 'matchrow']}
                )
        self.match_ids = [
                {'id': match.attrs.get('id')} for match in self.match_rows
                ]

    def __repr__(self):
        return 'Object is of class {}'.format(self.__class__.__name__)

    def show_matches(self, show_results=False):
        print(self.date_header[0].getText().upper())
        for match_row in self.match_rows:
            for match_id in self.match_ids:
                for key in match_id:
                    match_time = match_row.find(
                            'span', attrs={'id': 'ko'+match_id[key]}
                            )

                    match_title = match_row.find(
                            attrs={'id': 'g'+match_id[key]}
                            )
                    match_progress = match_row.find(
                            attrs={'id': 't'+match_id[key]}
                            )
                    if isinstance(match_time, Tag) and isinstance(
                            match_title, Tag
                            ):
                        d = datetime.strptime(
                            self.date + ' ' + match_time.getText(),
                            '%Y-%m-%d %I:%M%p'
                            )
                        est_d = LiveSoccerTV.est.localize(d)
                        pacific_d = est_d.astimezone(LiveSoccerTV.pacific)
                        formatted_pacific_d = datetime.strftime(
                            pacific_d, '%A, %d %B %Y %I:%M%p'
                            )

                        if show_results:
                            print(match_title.getText())
                            print(formatted_pacific_d, 'Pacific',
                                  match_progress.get(
                                          'title', match_progress.getText()
                                          ))
                            print('-'*80)
                        else:
                            print(match_title['title'])
                            print(formatted_pacific_d, 'Pacific',
                                  match_progress.get(
                                          'title', match_progress.getText()
                                          ))
                            print('-'*80)

# match_time['title'] if match is in the future
# id="koxxxxx" does not have attribute['title']


# test = LiveSoccerTV()
# test.show_matches()
