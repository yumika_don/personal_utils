class Person:

    def __init__(self, name, age, sex):
        self.name = name.title()
        self.age = age
        self.sex = sex


    # def __str__(self):
        # return 'this person\'s name is {}, age is {}, sex is {}'.format(self.name, self.age, self.sex)

    def __repr__(self):
        return '{} - {}'.format(self.__class__.__name__, self.name)


    def set_height(self, height):
        if height <= 0:
            raise ValueError('Your height needs to be greater than 0.')
        else:
            self.height = height
            return '{} is {}cm tall.'.format(self.name, self.height)

    def get_last_name(self):
        if self.name.split()[-1] == 'Don':
            return 'Don-don maru'
        else:
            return self.name.split()[-1]

    def __add__(self, other):
        return '{} and {}'.format(self.name, other)

    def speak(self, things_to_say=None):
        if things_to_say is None:
            return 'Got nutting to say.'
        else:
            self.things_to_say = things_to_say
            return self.things_to_say

    def spell_name(self):
        return [char for char in self.name if char != ' ']

class Daddy(Person):
    def __init__(self, name, age, sex, status=None):
        Person.__init__(self, name, age, sex)
        self.status = status

    def __repr__(self):
        return '{} is an instance of {}.'.format(self.name, self.__class__.__name__)

    def set_status(self, status):
        self.status = status
        return self.status

    def speak(self, stuff):
        return 'Bentancur'

    def take_care_of_kohana(self):
        return 'Kohana nene'

class C1:
    def meth1(self):
        self.__X = 88
    def meth2(self):
        print(self.__X)

class C2:
    def metha(self):
        self.__X = 99
    def methb(self):
        print(self.__X)

class C3(C1, C2):
    pass


if __name__ == '__main__':

    tri = Person('tri nguyen', 34, 'male')
    print(tri)
    print(tri.set_height(170))
    print(tri.speak('OOP is becoming a little clearer.'))
    print(tri.spell_name())
    print('Last Name:', tri.get_last_name())

    print()
    kohana_don = Person('kohana don', 2, 'female')
    print(kohana_don)
    print('Last Name:', kohana_don.get_last_name())
    print(kohana_don.speak('dadda'))
    print(kohana_don.spell_name())

    dad = Daddy('tri nguyen', 34, 'male', status='I am married')
    print(dad.status)
    print(dad.speak('I have 2 beautiful daughters.'))
    print(dad)
    print(dad.take_care_of_kohana())
    print(dad.set_status('Single'))

    for obj in (tri, kohana_don, dad):
        print(obj.speak('what is going on? Bentancur'))

    I = C3()
    I.meth1()
    I.metha()
    I.methb()
    I.meth2()
    print(I.__dict__)
    