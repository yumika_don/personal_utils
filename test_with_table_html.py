import requests
from datetime import datetime
from pytz import timezone
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString

html = 'table.html'
pacific = timezone('US/Pacific')
est = timezone('US/Eastern')
URL = r'https://www.livesoccertv.com/schedules/'


def show_matches(date_time):
    html = requests.get(URL+date_time)
    soup = BeautifulSoup(html.text, 'lxml')
    table_tag = soup.find('table', {'class': 'schedules'})
    tr_competitions = table_tag.find_all('tr', {'class': 'sortable_comp'})
    tr_matchrows = table_tag.find_all('tr', {'class': ['', 'matchrow']})
    event_time = table_tag.find_all('span', {'class': 'clickable'})
    tr_ids = [{'id': match.attrs.get('id')} for match in tr_matchrows]
    after_livecomp = tr_live.find_all_next('tr')
    print(tr_live.getText())

    for idx, tag in enumerate(after_livecomp):
        if tag.find('span', {'class': 'flag'}):
            print(tag.span.getText())
            print('-'*60)
        else:
            timecell = tag.find(attrs={'class': 'ts'})
            livecell = tag.find(attrs={'class': 'narrow'})
            game = tag.find('a')
            d = datetime.strptime(
                    tr_live.getText()+timecell.getText(),
                    '%A, %d %B %Y %I:%M%p'
                    )
            est_d = est.localize(d)
            pacific_d = est_d.astimezone(pacific)
            formatted_pacific_d = datetime.strftime(
                    pacific_d, '%A, %d %B %Y %I:%M%p'
                    )
            print(game.attrs.get('title'))
            print('Time:', formatted_pacific_d)
            print('Status:', livecell.getText())
            print('*'*60)

show_matches('2019-04-27')
# =============================================================================
# with open(html, 'r', encoding='utf-8') as html_in:
#     soup = BeautifulSoup(html_in, 'lxml')
#     table_tag = soup.find('table', {'class': 'schedules'})
#     tr_live = table_tag.find('tr', {'class': 'livecomp'})
#     tr_competitions = table_tag.find_all('tr', {'class': 'sortable_comp'})
#     tr_matchrows = table_tag.find_all('tr', {'class': ['', 'matchrow']})
#     event_time = table_tag.find_all('span', {'class': 'clickable'})
#     tr_ids = [{'id': match.attrs.get('id')} for match in tr_matchrows]
#     after_livecomp = tr_live.find_all_next('tr')
#     print(tr_live.getText())
# 
#     for idx, tag in enumerate(after_livecomp):
#         if tag.find('span', {'class': 'flag'}):
#             print(tag.span.getText())
#             print('-'*60)
#         else:
#             timecell = tag.find(attrs={'class': 'ts'})
#             livecell = tag.find(attrs={'class': 'narrow'})
#             game = tag.find('a')
#             d = datetime.strptime(
#                     tr_live.getText()+timecell.getText(),
#                     '%A, %d %B %Y %I:%M%p'
#                     )
#             est_d = est.localize(d)
#             pacific_d = est_d.astimezone(pacific)
#             formatted_pacific_d = datetime.strftime(
#                     pacific_d, '%A, %d %B %Y %I:%M%p'
#                     )
#             print(game.attrs.get('title'))
#             print('Time:', formatted_pacific_d)
#             print('Status:', livecell.getText())
#             print('*'*60)
# =============================================================================
# =============================================================================
#     for time in event_time:
#         print(time.attrs.get('onclick'))
# =============================================================================
# =============================================================================
#     for matchrow in tr_matchrows:
#         timecell = matchrow.find(attrs={'class': 'ts'})
#         livecell = matchrow.find(attrs={'class': 'narrow'})
#         game = matchrow.find('a')
#         d = datetime.strptime(tr_live.getText()+timecell.getText(),
#                               '%A, %d %B %Y %I:%M%p'
#                               )
#         est_d = est.localize(d)
#         pacific_d = est_d.astimezone(pacific)
#         formatted_pacific_d = datetime.strftime(pacific_d,
#                                                 '%A, %d %B %Y %I:%M%p')
#         print(game.attrs.get('title'))
#         print('Time:', formatted_pacific_d)
#         print('Status:', livecell.getText())
#         print('*'*60)
# =============================================================================
# =============================================================================
#     for idx, comp in enumerate(tr_competitions):
#         print(comp.span.getText())
#         for matchrow in tr_matchrows:
#             timecell = matchrow.find(attrs={'class': 'ts'})
#             livecell = matchrow.find(attrs={'class': 'narrow'})
#             game = matchrow.find('a')
#             d = datetime.strptime(
#                     tr_live.getText()+timecell.getText(),
#                     '%A, %d %B %Y %I:%M%p'
#                     )
#             est_d = est.localize(d)
#             pacific_d = est_d.astimezone(pacific)
#             formatted_pacific_d = datetime.strftime(
#                     pacific_d, '%A, %d %B %Y %I:%M%p'
#                     )
#             print(game.attrs.get('title'))
#             print('Time:', formatted_pacific_d)
#             print('Status:', livecell.getText())
#             print('*'*60)
# =============================================================================
