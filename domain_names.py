#records domain names instead of the address where the message was sent from instead of who the mail came from
#At the end, print out the contents of dictionary

file_name = input('Enter a file name: ')

try:
	file_handle = open(file_name)
except:
	print(file_name, ' does not exist.')
	exit()
	
domain_dict = dict()
delimiter = ' '

for line in file_handle:
	line = line.rstrip()
	if line.startswith('From'):
		words = line.split()
		if words[0] != 'From:' or len(words) == 0:
			continue
		else:
			contcat = delimiter.join(words)
			print(contcat)
			pos_at_sign= contcat.find('@')
			domain_name = contcat[pos_at_sign + 1:]
			print(domain_name)
			domain_dict.setdefault(domain_name, 0)
			domain_dict[domain_name] = domain_dict[domain_name] + 1
			
print(domain_dict)
		