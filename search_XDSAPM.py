file_name = input('Enter a file name: ')
file_handle = open(file_name)

def get_average(total_calc, total_items):
	average = total_calc / total_items
	return average

count = 0
confi_count = 0

for line in file_handle:
	if line.startswith('X-DSPAM-Confidence:'):
		count = count + 1 # count all the X-DSPAM-Confidence lines
		line = line.rstrip()
		colon_position = line.find(':')
		conf_number = line[colon_position + 1:] #extract floating point number, for exmple 0.8756
		conf_number = float(conf_number)
		confi_count =  confi_count + conf_number
		
print('Average spam confidence: ',get_average(confi_count, count))
		