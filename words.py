# read the words in words.txt and stores the mas keys in a dictionary.
# then use the in operator to check whether a string is in the dictionary

file_handle = open('words.txt')

words_dict = dict()

for line in file_handle:
	line = line.rstrip()
	words = line.split()
	for word in words:
		if word not in words_dict:
			words_dict[word] = word
				
print(words_dict)
print('you' in words_dict)