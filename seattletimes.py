import requests
from urllib.parse import urlparse
from bs4 import BeautifulSoup

url = 'https://www.seattletimes.com'


def get_top_stories_links():
    links = []
    r = requests.get(url)
    r.raise_for_status()
    html = r.text
    soup = BeautifulSoup(html, 'lxml')
    top_story_links = soup.find(
            attrs={'class': 'top-stories-at-top'}
            ).find_all('a')
    for link in top_story_links:
        parsed = urlparse(link.get('href'))
        if parsed[1] == 'www.seattletimes.com' and not parsed[2].startswith(
                '/sports'
                ):
            links.append(link)
    return links


def display_stories():
    links = get_top_stories_links()
    for link in links:
        r = requests.get(link.get('href'))
        r.raise_for_status()
        soup = BeautifulSoup(r.text, 'lxml')
        article_content = soup.find(
                attrs={'id': 'article-content'}
                ).get_text().strip()
        yield article_content


def display_individual_story(url):
    ''' parse url to display an individual story '''
    r = requests.get(url)
    r.raise_for_status()
    html = r.text
    soup = BeautifulSoup(html, 'lxml')
    article_title = soup.find(attrs={'class': 'article-title'})
    article_content = soup.find(attrs={'class': 'article-content'}).find_all(
        'p'
    )
    print(article_title.get_text().strip())
    for content in article_content:
        print(content.get_text())

