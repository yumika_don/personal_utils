# -*- coding: utf-8 -*-
"""
Created on Sun Sep 29 14:16:06 2019

@author: TriNguyen
"""

import difflib

mj1_one = 'HTML5-TB-V1-MJ1-MOBILE-'
mj1_two = 'HTML5-TB-V1-MJ1-MOBILE'

print(difflib.SequenceMatcher(None, mj1_one, mj1_two, False).ratio())
