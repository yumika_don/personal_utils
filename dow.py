# categorizes each mail message by which day of the week the commit was done
# look for lines that start with "From", then look for the 3rd word and keep a running count of each of the days of the week

file_name = input('Enter a file name: ')
try:
	file_handle = open(file_name)
except:
	print(file_name, ' does not exist')
	exit()
	
day_dict = dict()
count = 0

for line in file_handle:
	if line.startswith('From'):
		count = count + 1
		words = line.split()
		if words[0] != 'From' or len(words) == 0:
			continue
		else:
			day_dict.setdefault(words[2], 0)
			day_dict[words[2]] = day_dict[words[2]] + 1
				
for key in day_dict:
	if key in ('Thu', 'Fri', 'Sat'):
		print(key, ':', day_dict[key])

print(day_dict)
print('total number of lines that start with From is: ', count)

# for word in words:
	# if words[0] != 'From' or len(words) == 0:
		# continue
	# else:
		# day_dict.setdefault(word, 0)
		# day_dict[word] = day_dict[word] + 1
