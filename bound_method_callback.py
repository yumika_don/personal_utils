Python 3.6.0 (v3.6.0:41df79263a11, Dec 23 2016, 07:18:10) [MSC v.1900 32 bit (Intel)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> def square(arg):
	return arg ** 2

>>> class Sum:
	def __init__(self, val):
		self.val = val
	def __call__(self, arg):
		return self.val + arg

	
>>> class Product:
	def __init__(self, val):
		self.val = val
	def method(self, arg):
		return self.val * arg

	
>>> sum_obj = Sum(2)
>>> prod_obj = Product(3)
>>> actions = [square, sum_obj, prod_obj]
>>> for act in actions:
	print(act())

	
Traceback (most recent call last):
  File "<pyshell#20>", line 2, in <module>
    print(act())
TypeError: square() missing 1 required positional argument: 'arg'
>>> for act in actions:
	print(act(5))

	
25
7
Traceback (most recent call last):
  File "<pyshell#23>", line 2, in <module>
    print(act(5))
TypeError: 'Product' object is not callable
>>> new_acts = [square, sum_obj, prod_obj.method]
>>> for act in new_acts:
	print(act(5))

	
25
7
15
>>> [act(4) for act in new_acts]
[16, 6, 12]
>>> class Negate:
	def __init__(self, val):
		self.val = -val
	def __repr__(self):
		return str(self.val)

	
>>> new_acts.append(Negate)
>>> new_acts
[<function square at 0x0579D4F8>, <__main__.Sum object at 0x05791CB0>, <bound method Product.method of <__main__.Product object at 0x0532C310>>, <class '__main__.Negate'>]
>>> print(new_acts)
[<function square at 0x0579D4F8>, <__main__.Sum object at 0x05791CB0>, <bound method Product.method of <__main__.Product object at 0x0532C310>>, <class '__main__.Negate'>]
>>> for act in new_acts:
	print(act(5))

	
25
7
15
-5
>>> table = {act(5): act for act in new_acts}
>>> for (key, value) in table.items():
	print('{0:2} => {1}'.format(key, value))

	
25 => <function square at 0x0579D4F8>
 7 => <__main__.Sum object at 0x05791CB0>
15 => <bound method Product.method of <__main__.Product object at 0x0532C310>>
Traceback (most recent call last):
  File "<pyshell#44>", line 2, in <module>
    print('{0:2} => {1}'.format(key, value))
TypeError: unsupported format string passed to Negate.__format__
>>> 
