# write a program that looks for lines of the form: 'New Revision: 39772'
# extract the number from each of the lines using a regular expression and findall() method.
# compute the average of the numbers and print out the average.

import re

user_input = input('Enter a file: ')

file_open = open(user_input)


def get_average(list_of_nums):
	total = sum(list_of_nums)
	count = len(list_of_nums)
	average = total/count
	return average

rev_number_list = list()

for line in file_open:
	line = line.rstrip()
	rev_number = re.findall('^New Revision: ([0-9]+)', line)
	if len(rev_number) > 0:
		rev_number[0] = int(rev_number[0])
		rev_number_list.append(rev_number[0])

print(get_average(rev_number_list))