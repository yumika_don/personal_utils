#1. prompt user to enter the file name
#2. open the file
#3. read the file with for loop
#4. Find the line that starts with From and split that line into a list
#5. print out the the second index of the list to show who sent
#6. count the number of From not From: lines and print out a count at the end

file_name = input('Enter the name of the file: ')
try:
	file_handle = open(file_name)
except:
	print('File ', file_name, ' does not exist in this directory.')
	exit()
	
count = 0
for line in file_handle:
	words = line.split()
	if len(words) == 0 or words[0] != 'From':
		continue
	count = count + 1
	print(words[1].capitalize())
	
print('There were ', count, ' lines in the file with From as the first word.')