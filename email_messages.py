# write a program to read through a mail log, build a historam using a dictionary to count how many messages have come from
# each email address, and print the dictionary
#rewrite this program similarly to domain_names.py

file_name = input('Enter a file name: ')
try:
    file_handle = open(file_name)
except:
    print(file_name, ' does not exist.')
    exit()
    
email_dict = dict()

for line in file_handle:
    line = line.rstrip()
    if line.startswith('From'):
        words = line.split()
        print(words)
        for word in words:
            if words[0] != 'From:' or len(words) == 0:
                continue
            else:
                email_dict.setdefault(word, 0)
                email_dict[word] = email_dict[word] + 1
                
value_list = list(email_dict.values())
modified_value_list = value_list[1:]
                
for key in sorted(email_dict.keys()):
    if key == 'From:':
        continue
    else:
        print(key,':', email_dict[key])
        
for item in modified_value_list:
    if item == max(modified_value_list):
        for key in email_dict:
            if item == email_dict[key]:
                print('Most messages came from: ', key,'-',email_dict[key] )