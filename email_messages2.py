# read a and parse the "From" lines and pull out the addresses from the line.
# Count the number of messages from each person using a dictionary
# print the person with the most commits by creating a list of (count, email) tuples from the dictionary.
# sort the list in reverse order and print out the person who has the most commits.

file_name = input('Enter the file name: ')
try:
	file_handle = open(file_name)
except:
	print(file_name, ' does not exist.')
	exit()
	
email_dict = dict()

for line in file_handle:
	line = line.rstrip()
	if line.startswith('From'):
		words = line.split()
		if words[0] != 'From:' or len(words) == 0:
			continue
		else:
			email_dict.setdefault(words[1], 0)
			email_dict[words[1]] = email_dict[words[1]] + 1

list_of_email_dict = list()

for key, val in list(email_dict.items()):
	list_of_email_dict.append((val, key))

list_of_email_dict.sort(reverse=True)

print(list_of_email_dict)

print('The person who has the most commits is: ', list_of_email_dict[0][1], '-', list_of_email_dict[0][0])
