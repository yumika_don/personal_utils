import os
import re
import string
import time
from pprint import pprint

target_dir = r'C:\Program Files (x86)\Ampps\www\stuff\tutorials\python'
# target_dir = r'E:\tv_shows'

def rename_a_string(string):
    ''' take a string and replace space or special chars with underscore '''
    split_string = string.split()
    return '_'.join(split_string)


class RenameFiles:

    chars_to_repalce = ''.join([
            char for char in string.punctuation if char != '_'
            ])
    translation_table = str.maketrans(chars_to_repalce, '_'*31)

    def __init__(self, target_dir):
        self.target_dir = target_dir

    def get_folders(self):
        directory = os.listdir(self.target_dir)
        return [
                folder for folder in directory if os.path.isdir(
                        os.path.join(self.target_dir, folder)
                        )
                ]

    def get_files(self):
        directory = os.listdir(self.target_dir)
        return [
                file for file in directory if os.path.isfile(
                        os.path.join(self.target_dir, file)
                        )
                ]

    def rename_folders(self):
        folders = self.get_folders()
        for folder in folders:
            renamed_folder = folder.translate(RenameFiles.translation_table)
            # renamed_folder = renamed_folder.strip('_')
            renamed_folder = re.split(r'[_\s]', renamed_folder)
            clean_renamed_folder = '_'.join([
                    item for item in renamed_folder if item != ''
                    ])
            os.rename(
                    os.path.join(self.target_dir, folder),
                    os.path.join(self.target_dir, clean_renamed_folder)
                    )

    def rename_files(self):
        files = self.get_files()
        for file in files:
            name, ext = os.path.splitext(file)
            before = name.translate(RenameFiles.translation_table)
            before = re.split(r'[_\s]', before)
            after = '_'.join([
                    item for item in before if item != ''
                    ])
            os.rename(
                    os.path.join(self.target_dir, name+ext),
                    os.path.join(self.target_dir, after+ext)
                    )


test = RenameFiles(target_dir)
test.rename_files()
test.rename_folders()
# =============================================================================
# print(rename_a_string('Deep Learning With Python 3 Books in 1'))
# =============================================================================
# print('Done')
