import pathlib
import os

def get_root():
    root = os.makedirs(
        input('what is the full path where you like the project?')
    )
    return root

def main():
    ''' Entrypoint '''
    project_root = get_root()
    project_name = None
    while not project_name:
        project_name = input("What's the full name for the project?").strip()
    print('Creating {} in {}'.format(project_name, project_root))

if __name__ == '__main__':
    main()