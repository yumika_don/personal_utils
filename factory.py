>>> def factory(aClass, *pargs, **kargs):
	return aClass(*pargs, **kargs)

>>> class Spam:
	def doit(self, message):
		print(message)

		
>>> class Person:
	def __init__(self, name, job=None):
		self.name = name
		self.job = job

		
>>> obj_1 = factory(Spam)
>>> obj_2 = factory(Person, 'Arthur', 'King')
>>> obj_3 = factory(Person, name='Brian')
>>> obj_1.doit(99)
99
>>> obj_2.name, obj_2.job
('Arthur', 'King')
>>> obj_3.name, obj_3.job
('Brian', None)
>>> 
