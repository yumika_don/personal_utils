# -*- coding: utf-8 -*-
"""
Created on Thu May 17 22:49:23 2018

@author: TriNguyen
"""

from pprint import pprint
from collections import defaultdict
import random

donors = {

    'William Gates, III': [326892.24, 396892.24, 600],
    'Mark Zuckerberg': [546.37, 5485.37, 6465.37, 450],
    'Jeff Bezos': [877.33, 898],
    'Paul Allen': [235.14, 216.14, 296.14],
    'Tri Nguyen': [100, 89, 20],
    'Kohana Don': [50, 60, 70]
    }


def num_gifts(donations):
    return len(donations)

def average(donations):
    return sum(donations) / len(donations)

def total(donations):
    return sum(donations)

#for name in donor_dict:
#    donor_dict[name].append(random.randint(1, 100))
#    
#for name in donor_dict:
#    donor_dict[name].append(random.randint(1, 200))
    

def donor_db():
    return {name: {'total': total(donor_dict[name]),
                   'num_gifts': num_gifts(donor_dict[name]),
                   'avg': average(donor_dict[name])} for name in donor_dict}

def display_donor_list():
    ''' display list of donors
        from donor_dict '''
    for idx, name in enumerate(donor_dict.keys()):
        print(idx + 1, '-', name)

def challenge(factor=2, min_donation=0, max_donation=0):
    new_db = {}
    for name in donor_dict:
        new_db[name] = {'total': factor * total(donor_dict[name]),
                        'num_gifts': num_gifts(donor_dict[name]),
                        'avg': factor * average(donor_dict[name])}
    return new_db


def challenge_test(factor):
    new_donor_data = dict(list((key, list(map(lambda i:i*factor, value))) for key, value in donor_dict.items()))
    return {name: {'total': total(new_donor_data[name]),
                   'num_gifts': num_gifts(new_donor_data[name]),
                   'avg': average(new_donor_data[name])} for name in new_donor_data}, new_donor_data

dict_comp = {name: [item * 2 for item in donors[name] if item < 100] for name in donors}
current_db = {name: [item for item in donors[name] if item >= 100] for name in donors}

    
for name in current_db:
    current_db[name].extend(dict_comp[name])
    
pprint(current_db)

for name in current_db:
    print(name, '=', sum(current_db[name]))