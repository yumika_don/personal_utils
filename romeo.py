# 1. open romeo.txt
# 2. for each line in file, split it into list of words.
# 3. for each word in list of words, if word is not in list of words, append it to list of words.
# 4. sort and then print the sorted list.
try:
	romeo = open('romeo.txt')
except:
	print('File cannot be opened ', romeo)
	
list_of_words = []
for each_line in romeo:
	words = each_line.split()
	for word in words:
		if word not in list_of_words:
			list_of_words.append(word)

list_of_words.sort()
print(list_of_words)
		
	

